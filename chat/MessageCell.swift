//
//  MessageCell.swift
//  chat
//
//  Created by Руслан Ольховка on 25.12.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
	
	var space: CGFloat = 75

	var message = "Hello" {
		didSet {
			messageLabel?.text = message
		}
	}
	
	var own = true {
		didSet {
			baloon?.isOwnMessage = own
			leadingContraint.constant = own ? space : 0
			trailingConstraint.constant = own ? 0 : space
		}
	}
	
	@IBOutlet weak var baloon: BaloonView! {
		didSet {
			baloon.isOwnMessage = own
		}
	}
	@IBOutlet weak var messageLabel: UILabel! {
		didSet {
			messageLabel.text = message
		}
	}
	@IBOutlet weak var leadingContraint: NSLayoutConstraint! {
		didSet {
			leadingContraint.constant = own ? space : 0
		}
	}
	@IBOutlet weak var trailingConstraint: NSLayoutConstraint! {
		didSet {
			trailingConstraint.constant = own ? 0 : space
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
